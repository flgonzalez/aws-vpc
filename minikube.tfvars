aws_region = "us-east-2"
aws_zones = ["us-east-2a"]
#aws_zones = ["us-east-2a", "us-east-2b", "us-east-2c"]
vpc_name = "ferluko"
vpc_cidr = "10.14.0.0/16"
private_subnets = "false"

## Tags
tags = {
  App = "Minikube"
}
