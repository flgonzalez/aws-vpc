terraform {
  required_version = "~> 0.13.2"
  backend "http" {}
}

module "vpc" {
  source = "github.com/ferluko/terraform-aws-vpc"

  aws_region = "${var.aws_region}"
  aws_zones = "${var.aws_zones}"
  vpc_name = "${var.vpc_name}"
  vpc_cidr = "${var.vpc_cidr}"
  private_subnets = "${var.private_subnets}"

  ## Tags
  tags = "${var.tags}"
}
